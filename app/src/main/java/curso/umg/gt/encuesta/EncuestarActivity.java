package curso.umg.gt.encuesta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class EncuestarActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText et1,et2;
    private CheckBox chb1,chb2;
    Button bt1;
    TextView tv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuestar);

        et1=(EditText)findViewById(R.id.et1);
        et2=(EditText)findViewById(R.id.et2);
        bt1 = (Button)findViewById(R.id.bt1);
        tv6 = (TextView)findViewById(R.id.tv6);
        chb1=(CheckBox)findViewById(R.id.chb1);
        chb2=(CheckBox)findViewById(R.id.chb2);


        spinner = (Spinner) findViewById(R.id.spinner);
        String []opciones={"Ninguno","Natación","Fútbol","Voleibol","Baloncesto","Tenis"};
        ArrayAdapter <String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, opciones);
        spinner.setAdapter(adapter);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dato1 = et1.getText().toString();
                String dato2 = et2.getText().toString();

                String resu="";
                if (chb1.isChecked()==true) {
                    resu="SI te gusta algún deporte ";
                }
                if (chb2.isChecked()==true) {
                    resu="NO te gusta nigún deporte ";
                }

                String seleccionado = spinner.getSelectedItem().toString();

                tv6.setText("Hola " +dato1+" tu edad es: " +dato2+" años y "+resu+". Y tu deporte favorito es: "+seleccionado);
            }
        });
    }
}
